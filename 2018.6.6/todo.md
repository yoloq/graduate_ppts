# 找一些序列分析的软件

## 查看他们是如何使用的

* [statsmodels (a python package)](https://github.com/statsmodels/statsmodels)

* [PyFlux (Python包 未开发完全 提供了使用文档readdocs)](https://github.com/RJT1990/pyflux)

PyFlux is an open source time series library for Python. The library has a good array of modern time series models, as well as a flexible array of inference options (frequentist and Bayesian) that can be applied to these models. By combining breadth of models with breadth of inference, PyFlux allows for a probabilistic approach to time series modelling.

See some examples and documentation below. PyFlux is still only alpha software; this means you use it at your own risk, that test coverage is still in need of expansion, and also that some modules are still in need of being optimized.

Models
ARIMA models
ARIMAX models
Dynamic Autoregression models
Dynamic Paired Comparison models
GARCH models
Beta-t-EGARCH models
EGARCH-in-mean models
EGARCH-in-mean regression models
Long Memory EGARCH models
Skew-t-EGARCH models
Skew-t-EGARCH-in-mean models
GAS models
GASX models
GAS State Space models
Gaussian State Space models
Non-Gaussian State Space models
VAR modelss

* [sas 软件]()

* [Spass 软件]()

* [R 语言 有包]()

R 提供了很多可以使用的库(例如forcast)

I recommend R (see the time series view on CRAN).
https://cran.r-project.org/web/views/TimeSeries.html

Some useful references:

Econometrics in R, by Grant Farnsworth
**Multivariate** time series modelling in R
https://stackoverflow.com/questions/1714280/multivariate-time-series-modelling-in-r/1715488#1715488


* [LDT 软件](https://sourceforge.net/projects/letdatalk/)

You might want to consider using LDT. It is free and while it provides automatic forecasting with stationary vector autoregressive (VAR) models, you can benefit form other types of analysis.

* [Matlab](time series)

hile not exactly cheap, MATLAB is widely used in the financial industry for time series modelling





* [LSTM-NN-for Time Series Prediction using keras(a github rep)](https://github.com/jaungiers/LSTM-Neural-Network-for-Time-Series-Prediction)

* [Rep TF-Time-Series examples](https://github.com/hzy46/TensorFlow-Time-Series-Examples)

* [E-views 收费](http://www.eviews.com/home.html)

EViews offers academic researchers, corporations, government agencies, and students access to powerful statistical, forecasting, and modeling tools through an innovative, easy-to-use object-oriented interface.

* [RATS 收费]

* [时间序列分析方法](https://www.applysquare.com/topic-cn/SZica9z3N/)

* https://blog.csdn.net/LiuYuan_BJTU/article/details/67068404


* [jmp (收费)]()

* [Free Statistic and Forecasting Software这软件没下载地址？](https://www.wessa.net/stat.wasp)

* [MacAnova 很全面的一款时序分析工具 free with detailed docs](http://www.stat.umn.edu/macanova/capabilities.html)

* [Time Series Analysis and Forecasting with Weka 老离现在有4年](https://wiki.pentaho.com/display/DATAMINING/Time+Series+Analysis+and+Forecasting+with+Weka)




